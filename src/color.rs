use std::ops::{Add, Mul, Neg, Sub};

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct SGbra8([u8; 4]);

pub trait ColorSpace:
    Default
    + Add<Self, Output = Self>
    + Mul<f32, Output = Self>
    + Sub<Self, Output = Self>
    + Neg
    + Into<RGB>
    + From<RGB>
{
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct RGB {
    r: f32,
    g: f32,
    b: f32,
}

impl RGB {}

impl Default for RGB {
    fn default() -> Self {
        RGB {
            r: 0.0,
            g: 0.0,
            b: 0.0,
        }
    }
}

impl Add for RGB {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        RGB {
            r: self.r + rhs.r,
            g: self.g + rhs.g,
            b: self.b + rhs.b,
        }
    }
}

impl Sub for RGB {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        RGB {
            r: self.r - rhs.r,
            g: self.g - rhs.g,
            b: self.b - rhs.b,
        }
    }
}

impl Neg for RGB {
    type Output = Self;
    fn neg(self) -> Self {
        RGB {
            r: -self.r,
            g: -self.g,
            b: -self.b,
        }
    }
}

impl Mul<f32> for RGB {
    type Output = Self;
    fn mul(self, rhs: f32) -> Self {
        RGB {
            r: self.r * rhs,
            g: self.g * rhs,
            b: self.b * rhs,
        }
    }
}

impl ColorSpace for RGB {}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct CIELAB {
    l: f32,
    a: f32,
    b: f32,
}

impl Default for CIELAB {
    fn default() -> Self {
        CIELAB {
            l: 0.0,
            a: 0.0,
            b: 0.0,
        }
    }
}

impl Add for CIELAB {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        CIELAB {
            l: self.l + rhs.l,
            a: self.a + rhs.a,
            b: self.b + rhs.b,
        }
    }
}

impl Sub for CIELAB {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        CIELAB {
            l: self.l - rhs.l,
            a: self.a - rhs.a,
            b: self.b - rhs.b,
        }
    }
}

impl Neg for CIELAB {
    type Output = Self;
    fn neg(self) -> Self {
        CIELAB {
            l: -self.l,
            a: -self.a,
            b: -self.b,
        }
    }
}

impl Mul<f32> for CIELAB {
    type Output = Self;
    fn mul(self, rhs: f32) -> Self {
        CIELAB {
            l: self.l * rhs,
            a: self.a * rhs,
            b: self.b * rhs,
        }
    }
}

impl Into<RGB> for CIELAB {
    fn into(self) -> RGB {
        let y = (self.l + 16.0) / 116.0;
        let x = self.a / 500.0 + y;
        let z = y - self.b / 200.0;

        let x = 0.95047
            * if x * x * x > 0.008856 {
                x * x * x
            } else {
                (x - 16.0 / 116.0) / 7.787
            };
        let y = 1.0
            * if y * y * y > 0.008856 {
                y * y * y
            } else {
                (y - 16.0 / 116.0) / 7.787
            };
        let z = 1.08883
            * if z * z * z > 0.008856 {
                z * z * z
            } else {
                (z - 16.0 / 116.0) / 7.787
            };

        let r = x * 3.2406 + y * -1.5372 + z * -0.4986;
        let g = x * -0.9689 + y * 1.8758 + z * 0.0415;
        let b = x * 0.0557 + y * -0.2040 + z * 1.0570;

        let r = if r > 0.0031307 {
            1.055 * r.powf(1.0 / 2.4) - 0.055
        } else {
            12.92 * r
        };
        let g = if g > 0.0031307 {
            1.055 * g.powf(1.0 / 2.4) - 0.055
        } else {
            12.92 * g
        };
        let b = if b > 0.0031307 {
            1.055 * b.powf(1.0 / 2.4) - 0.055
        } else {
            12.92 * b
        };
        RGB { r, g, b }
    }
}

impl From<RGB> for CIELAB {
    fn from(rgb: RGB) -> Self {
        let r = rgb.r;
        let g = rgb.g;
        let b = rgb.b;
        todo!()
    }
}

impl ColorSpace for CIELAB {}

pub struct AlphaSpace<CS> {
    pub color: CS,
    pub alpha: f32,
}

impl<CS: ColorSpace> AlphaSpace<CS> {
    fn canonical(self) -> AlphaSpace<RGB> {
        let rgb: RGB = self.color.into();
        AlphaSpace {
            color: rgb,
            alpha: self.alpha,
        }
    }
}

impl<CS: ColorSpace> From<AlphaSpace<CS>> for SGbra8 {
    fn from(alpha_space: AlphaSpace<CS>) -> Self {
        let rgb: RGB = alpha_space.color.into();
        let gamma = 1.0 / 2.2;
        let r = (rgb.r.powf(gamma) * 255.0) as u8;
        let g = (rgb.g.powf(gamma) * 255.0) as u8;
        let b = (rgb.b.powf(gamma) * 255.0) as u8;
        let a = (alpha_space.alpha * 255.0) as u8;

        SGbra8([g, b, r, a])
    }
}

impl<CS: ColorSpace> From<CS> for SGbra8 {
    fn from(cs: CS) -> Self {
        let rgb: RGB = cs.into();
        let gamma = 1.0 / 2.2;
        let r = (rgb.r.powf(gamma) * 255.0) as u8;
        let g = (rgb.g.powf(gamma) * 255.0) as u8;
        let b = (rgb.b.powf(gamma) * 255.0) as u8;
        let a = 255;

        SGbra8([g, b, r, a])
    }
}
