#![feature(slice_as_chunks)]

use chrono::prelude::*;
use hex::decode;
use num::complex::{Complex64, ComplexFloat};
use rayon::prelude::*;
use sdl2::event::{Event, WindowEvent};
use sdl2::keyboard::Keycode;
use sdl2::mouse::{MouseState, MouseWheelDirection};
use sdl2::sys::__builtin_va_list;
use std::cmp::Ordering;
use std::io::BufWriter;
use std::rc::Rc;
use std::sync::{Arc, Mutex, RwLock};
use std::time;

use num::{Complex, Zero};

mod color;
mod geom;

use geom::*;

const ENCODE_GAMMA: f32 = 1.0 / 2.2;
const DECODE_GAMMA: f32 = 2.2;

pub trait Format:
    Default
    + Clone
    + Copy
    + std::ops::Add<Self, Output = Self>
    + std::ops::Mul<f64, Output = Self>
    + 'static
{
}

#[derive(Debug, Clone, Copy, PartialOrd, PartialEq)]
pub struct RGBA([f32; 4]);

impl RGBA {
    const BLACK: RGBA = RGBA([0.0, 0.0, 0.0, 1.0]);
    const WHITE: RGBA = RGBA([1.0, 1.0, 1.0, 1.0]);
    const RED: RGBA = RGBA([1.0, 0.0, 0.0, 1.0]);
    const GREEN: RGBA = RGBA([0.0, 1.0, 0.0, 1.0]);
    const BLUE: RGBA = RGBA([0.0, 0.0, 1.0, 1.0]);
    const YELLOW: RGBA = RGBA([1.0, 1.0, 0.0, 1.0]);
    const INVISIBLE: RGBA = RGBA([0.0, 0.0, 0.0, 0.0]);

    pub fn new(r: f32, g: f32, b: f32, a: f32) -> Self {
        RGBA([r, g, b, a])
    }

    pub fn bytes(r: u8, g: u8, b: u8, a: u8) -> Self {
        RGBA([
            (r as f32) / 255.0,
            (g as f32) / 255.0,
            (b as f32) / 255.0,
            (a as f32) / 255.0,
        ])
    }

    pub fn hex(mut val: &str) -> Self {
        if val.starts_with("#") {
            val = &val[1..];
        }
        let rgb_a = hex::decode(val).unwrap();
        match &rgb_a[..] {
            [r, g, b, a] => RGBA::bytes(*r, *g, *b, *a),
            [r, g, b] => RGBA::bytes(*r, *g, *b, 255),
            _ => panic!(),
        }
    }
}

impl Default for RGBA {
    fn default() -> Self {
        RGBA::INVISIBLE
    }
}

impl Format for RGBA {}

pub trait Blend {
    fn blend(&self, other: &Self) -> Self;
}

impl Blend for RGBA {
    fn blend(&self, other: &Self) -> Self {
        let over_alpha = self.0[3];
        let under_alpha = other.0[3];
        let one_minus_over_alpha = 1.0 - over_alpha;
        let out_alpha = over_alpha + under_alpha * (1.0 - over_alpha);
        if out_alpha <= 0.001 {
            return RGBA([0.0, 0.0, 0.0, 0.0]);
        }
        RGBA([
            (self.0[0] * over_alpha + other.0[0] * under_alpha * one_minus_over_alpha) / out_alpha,
            (self.0[1] * over_alpha + other.0[1] * under_alpha * one_minus_over_alpha) / out_alpha,
            (self.0[2] * over_alpha + other.0[2] * under_alpha * one_minus_over_alpha) / out_alpha,
            out_alpha,
        ])
    }
}

impl std::ops::Add for RGBA {
    type Output = RGBA;
    fn add(self, rhs: RGBA) -> RGBA {
        RGBA([
            (self.0[0] + rhs.0[0]),
            (self.0[1] + rhs.0[1]),
            (self.0[2] + rhs.0[2]),
            (self.0[3] + rhs.0[3]),
        ])
    }
}

impl std::ops::Mul<f32> for RGBA {
    type Output = RGBA;
    fn mul(self, rhs: f32) -> RGBA {
        RGBA([
            self.0[0] * rhs,
            self.0[1] * rhs,
            self.0[2] * rhs,
            self.0[3] * rhs,
        ])
    }
}

impl std::ops::Mul<f64> for RGBA {
    type Output = RGBA;
    fn mul(self, rhs: f64) -> RGBA {
        let rhs = rhs as f32;
        RGBA([
            self.0[0] * rhs,
            self.0[1] * rhs,
            self.0[2] * rhs,
            self.0[3] * rhs,
        ])
    }
}

pub trait Image<Samp, Out> {
    fn sample(&self, p: Samp) -> Out;
}

impl<F, Samp, Out: Clone> Image<Samp, Out> for F
where
    F: Fn(Samp) -> Out,
{
    fn sample(&self, p: Samp) -> Out {
        self(p)
    }
}

pub trait Region<Sample>: Image<Sample, bool> {}

impl<Sample, T: Image<Sample, bool>> Region<Sample> for T {}

pub fn color_disc(
    color: RGBA,
    pos: CPoint,
    radius: f64,
) -> impl Fn(CPoint) -> RGBA + Clone + 'static {
    move |p: CPoint| -> RGBA {
        if (pos.x - p.x) * (pos.x - p.x) + (pos.y - p.y) * (pos.y - p.y) <= radius * radius {
            color
        } else {
            RGBA([0.0, 0.0, 0.0, 0.0])
        }
    }
}

pub fn disc(pos: CPoint, radius: f64) -> impl Fn(CPoint) -> RGBA + Clone + 'static {
    color_disc(RGBA::BLACK, pos, radius)
}

pub fn white(_: CPoint) -> RGBA {
    RGBA::WHITE
}

pub fn black(_: CPoint) -> RGBA {
    RGBA::BLACK
}

pub fn color<F: Format>(c: F) -> impl Fn(CPoint) -> F + Clone + 'static {
    let c = c.clone();
    move |_: CPoint| c.clone()
}

pub fn distance_to_origin(p: CPoint) -> f64 {
    (p.x * p.x + p.y * p.y).sqrt()
}

pub fn wav_dist(p: CPoint) -> f64 {
    (1.0 + (std::f64::consts::PI * distance_to_origin(p)).cos()) * 0.5
}

pub fn blend<S: Sample, F: Format + Blend, I1: Image<S, F> + Clone, I2: Image<S, F> + Clone>(
    a: I1,
    b: I2,
) -> impl Fn(S) -> F + Clone {
    move |p: S| {
        let va = a.sample(p);
        let vb = b.sample(p);
        va.blend(&vb)
    }
}

pub fn lerp_color<F: Format>(c1: F, c2: F, t: f64) -> F {
    c1 * (1.0 - t) + c2 * t
}

pub fn lerp<
    S: Sample,
    F: Format,
    I1: Image<S, F> + Clone,
    I2: Image<S, F> + Clone,
    Func: Image<S, f64> + Clone,
>(
    func: Func,
    image1: I1,
    image2: I2,
) -> impl Fn(S) -> F + Clone {
    move |p: S| lerp_color(image1.sample(p), image2.sample(p), func.sample(p))
}

pub fn bilerp_color<F: Format>(c1: F, c2: F, c3: F, c4: F) -> impl Fn(CPoint) -> F + Clone {
    move |p: CPoint| lerp_color(lerp_color(c1, c2, p.x), lerp_color(c3, c4, p.x), p.y)
}

pub fn gradient<'a, F: Format, const N: usize>(
    points: [(f64, F); N],
) -> impl Fn(f64) -> F + Clone + 'a {
    move |f: f64| {
        let (left, right) = match points.binary_search_by(|(a, _)| {
            if a < &f {
                Ordering::Less
            } else if a > &f {
                Ordering::Greater
            } else if a == &f {
                Ordering::Equal
            } else {
                Ordering::Less
            }
        }) {
            Ok(idx) => {
                let idx = idx.clamp(1, points.len() - 1);
                (idx - 1, idx)
            }
            Err(idx) => {
                let idx = idx.clamp(1, points.len() - 1);
                (idx - 1, idx - 0)
            }
        };
        let width = points[right].0 - points[left].0;
        let t = (f - points[left].0) / width;
        lerp_color(points[left].1, points[right].1, t as f64)
    }
}

pub fn simple_gradient<F: Format>(start: F, end: F) -> impl Fn(f64) -> F + Clone {
    move |f: f64| lerp_color(start, end, f)
}

pub fn horizontal_strip<'a, F: Format, G: Fn(f64) -> F + Clone + 'a>(
    grad: G,
) -> impl Image<CPoint, F> + Clone + 'a {
    move |p: CPoint| grad((p.x + 1.0) * 0.5)
}

pub fn local_avg<
    Format: std::ops::Add<Output = Format> + std::ops::Mul<f32, Output = Format>,
    I: Image<CPoint, Format> + Clone,
>(
    image: I,
    dx: f64,
    dy: f64,
) -> impl Fn(CPoint) -> Format + Clone {
    move |p: CPoint| {
        let dx = dx * 0.5;
        let dy = dy * 0.5;
        let ul = image.sample(CPoint::new(p.x - dx, p.y + dy));
        let ur = image.sample(CPoint::new(p.x + dx, p.y + dy));
        let ll = image.sample(CPoint::new(p.x - dx, p.y - dy));
        let lr = image.sample(CPoint::new(p.x + dx, p.y - dy));
        (ul + ur + ll + lr) * 0.25
    }
}

pub fn mandelbrot<'a, F: Format, G: Fn(f64) -> F + Clone + 'a>(
    g: G,
) -> impl Fn(CPoint) -> F + Clone + 'a {
    const NUM_ITERS: usize = 400;
    move |p: CPoint| {
        let c: Complex64 = Complex::new(p.x, p.y);
        let mut z: Complex64 = Complex::zero();
        let mut i = 0;
        let f = loop {
            if i == NUM_ITERS {
                break 1.0;
            };
            z = z * z + c;
            // let abs = z.abs();
            if (z * z.conj()).re() > 4.0 {
                // if abs > 2.0 {
                break (i as f64) / (NUM_ITERS as f64);
            };
            i += 1;
        };
        g(f)
    }
}

pub fn mandelbrot_n<'a, F: Format, G: Fn(f64) -> F + Clone + 'a>(
    g: G,
    n: usize,
) -> impl Fn(CPoint) -> F + Clone + 'a {
    // const NUM_ITERS: usize = 400;
    move |p: CPoint| {
        let c: Complex64 = Complex::new(p.x, p.y);
        let mut z: Complex64 = Complex::zero();
        let mut i = 0;
        let f = loop {
            if i == n {
                break 1.0;
            };
            z = z * z + c;
            // let abs = z.abs();
            if (z * z.conj()).re() > 4.0 {
                // if abs > 2.0 {
                break (i as f64) / (n as f64);
            };
            i += 1;
        };
        g(f)
    }
}
pub fn julia<'a, F: Format, G: Fn(f64) -> F + Clone + 'a>(
    c: CPoint,
    g: G,
) -> impl Fn(CPoint) -> F + Clone + 'a {
    const NUM_ITERS: usize = 100;
    move |p: CPoint| {
        let c: Complex64 = Complex::new(c.x, c.y);
        let mut z: Complex64 = Complex::new(p.x, p.y);
        let mut i = 0;
        let f = loop {
            if i == NUM_ITERS {
                break 1.0;
            };
            z = z * z + c;
            if (z * z.conj()).re() > 100.0 {
                // if z.abs() > 10.0 {
                break (i as f64) / (NUM_ITERS as f64);
            };
            i += 1;
        };
        g(f)
    }
}

#[derive(Debug, Clone)]
pub struct Poly {
    coeffs: Vec<f64>,
}

impl Poly {
    fn new(mut coeffs: &[f64]) -> Self {
        loop {
            if let Some(x) = coeffs.last() {
                if x.is_zero() {
                    coeffs = &coeffs[0..coeffs.len() - 1];
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        if coeffs.len() == 0 {
            coeffs = &[0.0]
        }

        Poly {
            coeffs: coeffs.into(),
        }
    }

    fn eval(&self, x: f64) -> f64 {
        let mut tally = self.coeffs[0];
        let mut pow = x;
        for coeff in &self.coeffs[1..] {
            tally += coeff * pow;
            pow *= x;
        }
        tally
    }

    fn deriv(&self) -> Poly {
        let mut out = Vec::new();
        out.resize(self.coeffs.len() - 1, 0.0);

        for (idx, val) in out.iter_mut().enumerate() {
            *val = self.coeffs[idx + 1] * (idx + 1) as f64;
        }

        /*let mut carry = self.coeffs.last().unwrap() * (self.coeffs.len() - 1) as f64;

        for (i, val) in out.iter_mut().enumerate().rev().skip(1) {
            *val = carry;
            carry = self.coeffs[i] * i as f64;
        }

        for i in (0..out.len()).into_iter().skip(1).rev() {
            if out[i].is_zero() {
                out.pop();
            }
        }*/

        Poly { coeffs: out }
    }

    fn div(&self, p: &Poly) -> Poly {
        todo!()
    }

    fn find_roots(&self) -> Vec<f64> {
        todo!()
    }

    fn try_find_root(p: Poly) -> f64 {
        todo!()
    }
}

impl std::fmt::Display for Poly {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (idx, a) in self.coeffs[0..].iter().enumerate().skip(1).rev() {
            write!(f, "{}{}x^{}", if a < &0.0 { "" } else { "+" }, a, idx)?;
        }
        write!(
            f,
            "{}{}",
            if self.coeffs[0] < 0.0 || self.coeffs.len() == 1 {
                ""
            } else {
                "+"
            },
            self.coeffs[0]
        )
    }
}

#[derive(Debug, Clone)]
pub struct Polynomial {
    roots: Vec<Complex64>,
}

impl Polynomial {
    fn new(roots: &[Complex64]) -> Self {
        let mut roots_vec = Vec::new();
        roots_vec.extend_from_slice(roots);
        Polynomial { roots: roots_vec }
    }

    fn eval(&self, point: Complex64) -> Complex64 {
        let mut tally = Complex64::new(1.0, 0.0);
        for root in &self.roots[..] {
            tally *= point - root;
        }
        tally
    }

    fn closest_root(&self, point: Complex64) -> (Complex64, usize) {
        assert!(self.roots.len() >= 1);
        let mut closest = (self.roots[0], 0);
        for (idx, root) in self.roots.iter().enumerate() {
            if (point - root).abs() < (point - closest.0).abs() {
                closest = (*root, idx);
            }
        }
        closest
    }
}

pub fn newton_raphson<'a, F: Format, G: Fn(f32) -> F + 'a>(
    g: G,
    poly: Polynomial,
    iters: usize,
) -> impl Fn(CPoint) -> F + 'a {
    let delta = 0.001;
    let dx = Complex64::new(delta, 0.0);
    let over_dx = 1.0 / dx;
    move |p: CPoint| {
        let mut x = Complex64::new(p.x as f64, p.y as f64);
        for _ in 0..iters {
            let fx = poly.eval(x);
            let fx_dx = poly.eval(x + dx);
            let d_fx = (fx_dx - fx) * over_dx;
            x = x - fx / d_fx;
            if fx.abs() < delta {
                break;
            }
        }
        let (closest_root, closest_idx) = poly.closest_root(x);
        g(closest_idx as f32 + (x - closest_root).abs() as f32)
    }
}

pub fn scale<'a, S: Copy + std::ops::Mul<f64, Output = S>, F, I: Image<S, F> + 'a>(
    image: &'a I,
    s: f64,
) -> impl Fn(S) -> F + 'a {
    move |p: S| {
        image.sample(p * (1.0 / s))
        /*image.sample(CPoint {
            x: p.x / s,
            y: p.y / s,
        })*/
    }
}

pub fn translate<
    'a,
    S: std::ops::Sub<S, Output = S> + Copy + 'a,
    F: Clone + 'a,
    I: Image<S, F> + 'a,
>(
    image: &'a I,
    tr: S,
) -> impl Fn(S) -> F + 'a {
    move |p: S| {
        image.sample(p - tr)
        /*image.sample(CPoint {
            x: p.x - tr.x,
            y: p.y - tr.y,
        })*/
    }
}

pub fn frame<'a, F: Format, I: Image<CPoint, F> + 'a>(
    image: &'a I,
    lower_left: CPoint,
    upper_right: CPoint,
) -> impl Fn(CPoint) -> F + 'a {
    let mid = CPoint {
        x: (lower_left.x + upper_right.x) * 0.5,
        y: (lower_left.y + upper_right.y) * 0.5,
    };
    let s = 2.0 / (upper_right.y - lower_left.y);
    move |p: CPoint| {
        let inner = scale(image, s);
        let frame_transform = translate(&inner, mid);
        if (p.x >= lower_left.x)
            && (p.x <= upper_right.x)
            && (p.y >= lower_left.y)
            && (p.y <= upper_right.y)
        {
            frame_transform.sample(p).into()
        } else {
            F::default()
        }
    }
}

pub fn crop<'a, S: Sample, F: Default + Clone + 'a, I: Image<S, F> + 'a, R: Region<S> + 'a>(
    reg: R,
    image: I,
) -> impl Image<S, F> + 'a {
    move |p: S| {
        if reg.sample(p) {
            image.sample(p)
        } else {
            F::default()
        }
    }
}

pub fn mix<
    'a,
    S: Sample,
    F: Clone,
    I1: Image<S, F> + 'a,
    I2: Image<S, F> + 'a,
    R: Region<S> + 'a,
>(
    reg: R,
    image1: I1,
    image2: I2,
) -> impl Image<S, F> + 'a {
    move |p: S| {
        if reg.sample(p) {
            image1.sample(p)
        } else {
            image2.sample(p)
        }
    }
}

pub fn or<'a, S: Sample, R1: Region<S> + 'a, R2: Region<S> + 'a>(
    r1: R1,
    r2: R2,
) -> impl Region<S> + 'a {
    move |p| r1.sample(p) || r2.sample(p)
}

pub fn horizontal_slice(width: f64) -> impl Region<CPoint> + Clone {
    move |p: CPoint| {
        if p.x.abs() <= width * 0.5 {
            true
        } else {
            false
        }
    }
}

pub fn checkerboard(p: CPoint) -> bool {
    ((p.x.floor() as i32) + (p.y.floor() as i32)) % 2 == 0
}

pub fn alternating_rings(p: CPoint) -> bool {
    (distance_to_origin(p).floor() as i32) % 2 == 0
}

pub fn vertical<'a, F: Format, I1: Image<CPoint, F> + 'a, I2: Image<CPoint, F> + 'a>(
    upper: &'a I1,
    lower: &'a I2,
) -> impl Fn(CPoint) -> F + 'a {
    let upper_frame = frame(upper, CPoint::new(-1.0, 0.0), CPoint::new(1.0, 1.0));
    let lower_frame = frame(lower, CPoint::new(-1.0, -1.0), CPoint::new(1.0, 0.0));
    move |p: CPoint| {
        if p.y >= 0.0 {
            upper_frame.sample(p)
        } else {
            lower_frame.sample(p)
        }
    }
}

pub fn horizontal<'a, F: Format, I1: Image<CPoint, F> + 'a, I2: Image<CPoint, F> + 'a>(
    left: I1,
    right: I2,
) -> impl Fn(CPoint) -> F + 'a {
    move |p: CPoint| {
        let left_frame = frame(&left, CPoint::new(-1.0, -1.0), CPoint::new(0.0, 1.0));
        let right_frame = frame(&right, CPoint::new(0.0, -1.0), CPoint::new(1.0, 1.0));
        if p.x >= 0.0 {
            right_frame.sample(p)
        } else {
            left_frame.sample(p)
        }
    }
}

pub fn quad<
    'a,
    F: Format,
    I1: Image<CPoint, F> + 'a,
    I2: Image<CPoint, F> + 'a,
    I3: Image<CPoint, F> + 'a,
    I4: Image<CPoint, F> + 'a,
>(
    f1: &'a I1,
    f2: &'a I2,
    f3: &'a I3,
    f4: &'a I4,
) -> impl Image<CPoint, F> + 'a {
    // let img = vertical(horizontal(f1, f2), horizontal(f3, f4));
    let v1 = vertical(f1, f3);
    let v2 = vertical(f2, f4);
    move |p: CPoint| {
        let img = horizontal(&v1, &v2);

        img.sample(p)
    }
}

pub fn from_polar<'a, F: Clone, I: Image<PPoint, F> + 'a>(img: I) -> impl Image<CPoint, F> + 'a {
    move |p: CPoint| img.sample(p.into())
}

pub fn to_polar<'a, F: Clone, I: Image<CPoint, F> + 'a>(img: I) -> impl Image<PPoint, F> + 'a {
    move |p: PPoint| img.sample(p.into())
}

pub fn polar_trans<
    F: Clone,
    I1: Image<PPoint, F> + Clone,
    I2: Image<PPoint, F> + Clone,
    Trans: Fn(I1) -> I2,
    I: Image<PPoint, F> + Clone,
>(
    transform: Trans,
    image: I1,
) -> impl Image<CPoint, F> + Clone {
    let img = transform(image);
    |p: CPoint| {
        let p: PPoint = p.into();
        todo!()
    }
}

pub fn write_image_to_sdl2_texture<
    'a,
    F: Format + Into<RGBA>,
    I: Image<CPoint, F> + Send + Sync + 'a,
>(
    image: &'a I,
    texture: &mut sdl2::render::Texture,
    region: Quad,
) {
    let q = texture.query();
    let t = Texture::from_sampled_image::<_, _, SBgra8>(
        q.width as usize,
        q.height as usize,
        image,
        region,
    );

    texture
        .update(None, &t.bytes[..], (q.width * 4) as usize)
        .unwrap();
}

#[derive(Debug, Clone)]
pub struct Texture {
    pub width: usize,
    pub height: usize,
    pub bytes: Vec<u8>,
}

#[derive(Debug, Clone)]
pub struct SBgra8([u8; 4]);

#[derive(Debug, Clone)]
pub struct SRgba8([u8; 4]);

pub trait PixelFormat {
    fn from_rgba(from: RGBA) -> Self;
    fn as_slice(&self) -> &[u8];
    fn data(&self) -> [u8; 4];
}

impl PixelFormat for SBgra8 {
    // #[inline(never)]
    fn from_rgba(from: RGBA) -> Self {
        // let r = (from.0[0].powf(ENCODE_GAMMA) * 255.0) as u8;
        // let g = (from.0[1].powf(ENCODE_GAMMA) * 255.0) as u8;
        // let b = (from.0[2].powf(ENCODE_GAMMA) * 255.0) as u8;
        // let a = (from.0[3] * 255.0) as u8;
        let r = (from.0[0].sqrt() * 255.0) as u8;
        let g = (from.0[1].sqrt() * 255.0) as u8;
        let b = (from.0[2].sqrt() * 255.0) as u8;
        let a = (from.0[3] * 255.0) as u8;
        SBgra8([b, g, r, a])
    }

    fn as_slice(&self) -> &[u8] {
        &self.0
    }

    fn data(&self) -> [u8; 4] {
        self.0
    }
}

impl PixelFormat for SRgba8 {
    // #[inline(never)]
    fn from_rgba(from: RGBA) -> Self {
        let r = (from.0[0].powf(ENCODE_GAMMA) * 255.0) as u8;
        let g = (from.0[1].powf(ENCODE_GAMMA) * 255.0) as u8;
        let b = (from.0[2].powf(ENCODE_GAMMA) * 255.0) as u8;
        let a = (from.0[3] * 255.0) as u8;
        SRgba8([r, g, b, a])
    }

    fn as_slice(&self) -> &[u8] {
        &self.0
    }

    fn data(&self) -> [u8; 4] {
        self.0
    }
}

/*fn from_sampled_image<
    'a,
    Format: Into<RGBA>,
    I: Image<CPoint, Format> + Send + Sync + 'a,
    Out: PixelFormat,
>(
    width: usize,
    height: usize,
    image: &'a I,
    region: Quad,
    gamma: f32,
    ssaa: bool,
) -> Self {
    let dy = 1.0 / (height as f64);
    let dx = 1.0 / (width as f64);
    let image = Arc::new(image);
    let mut bytes: Vec<u8> = Vec::new();
    bytes.resize(4 * height * width, 0);
    bytes
        // .par_chunks_exact_mut(width * 4)
        .chunks_exact_mut(width * 4)
        .enumerate()
        .for_each(
            |(h, line)| {
                let ty = (h as f64 + 0.5) * dy;
                // unsafe {
                for (w, pixel) in line.as_chunks_mut::<4>().0.iter_mut().enumerate() {
                    let tx = (w as f64 + 0.5) * dx;

                    // no ssaa

                    /*if ssaa {
                        let p1 = region.sample(tx - dx * 0.25, ty - dy * 0.25);
                        let p2 = region.sample(tx + dx * 0.25, ty - dy * 0.25);
                        let p3 = region.sample(tx - dx * 0.25, ty + dy * 0.25);
                        let p4 = region.sample(tx + dx * 0.25, ty + dy * 0.25);
                        let color1: RGBA = image.sample(p1).into();
                        let color2: RGBA = image.sample(p2).into();
                        let color3: RGBA = image.sample(p3).into();
                        let color4: RGBA = image.sample(p4).into();
                        let out: Out =
                            Out::from_rgba((color1 + color2 + color3 + color4) * 0.25);
                        *pixel = out.data();
                    */// } else {
                    let p = region.sample(tx, ty);
                    let color: RGBA = image.sample(p).into();
                    let out: Out = Out::from_rgba(color);
                    // let data = out.data();
                    let data = out.as_slice();
                    *pixel = [data[0], data[1], data[2], data[3]];
                    // *pixel = [out.as_slice()[0], 0, 0, 0]; // out.data();
                    // }
                }
            }, // }
        );

    Texture {
        width,
        height,
        bytes,
    }
}*/

impl Texture {
    fn from_sampled_image<
        'a,
        Format: Into<RGBA>,
        I: Image<CPoint, Format> + Send + Sync + 'a,
        Out: PixelFormat,
    >(
        width: usize,
        height: usize,
        image: &'a I,
        region: Quad,
    ) -> Self {
        let dy = 1.0 / (height as f64);
        let dx = 1.0 / (width as f64);
        let image = Arc::new(image);

        let mut bytes: Vec<u8> = Vec::new();
        bytes.resize(4 * height * width, 0);
        bytes
            .par_chunks_exact_mut(width * 4)
            // .chunks_exact_mut(width * 4)
            .enumerate()
            .for_each(|(h, line)| {
                let ty = (h as f64 + 0.5) * dy;
                for (w, pixel /*&mut [u8; 4]*/) in
                    line.as_chunks_mut::<4>().0.iter_mut().enumerate()
                {
                    let tx = (w as f64 + 0.5) * dx;
                    let p = region.sample(tx, ty);
                    let color: RGBA = image.sample(p).into(); // expensive

                    let out: Out = Out::from_rgba(color); // expensive, function marked with #[inline(never)]
                                                          //let data: [u8; 4] = out.data();
                    *pixel = out.data();

                    // different ways to fill the pixel with data, measured as full frame time (800*800 pixels)
                    // *pixel = [0, 0, 0, 0]; // 0-1ms, uninteresting (probably optimized away)
                    // *pixel = [data[0], 0, 0, 0]; // 2-3ms
                    // *pixel = [data[0], data[1], 0, 0]; // 4-5ms
                    // *pixel = [data[0], data[1], data[2], 0]; // 6-8ms
                    // *pixel = [data[0], data[1], data[2], data[3]]; // 12ms
                    // *pixel = data; // 12ms
                    // pixel[0] = data[0]; // 2-3ms
                    // pixel[1] = data[1]; // 4-5ms (cumulative), 2ms alone
                    // pixel[2] = data[2]; // 6-7ms (cumulative), 2ms alone
                    // pixel[3] = data[3]; // 12-13ms (cumulative), 6-8ms alone!!!
                }
            });

        Texture {
            width,
            height,
            bytes,
        }
    }

    pub fn write_to_file<'a, S: std::ops::Deref<Target = str> + 'a>(&self, path: S) {
        let path: &str = path.deref();
        let path = std::path::Path::new(path);
        let file = std::fs::File::create(path).unwrap();
        let ref mut w = BufWriter::new(file);

        let mut encoder = png::Encoder::new(w, self.width as u32, self.height as u32);
        encoder.set_color(png::ColorType::Rgba);
        encoder.set_depth(png::BitDepth::Eight);
        encoder.set_source_gamma(png::ScaledFloat::new(1.0 / 2.2));

        let mut writer = encoder.write_header().unwrap();

        writer.write_image_data(&self.bytes).unwrap();
    }
}

#[derive(Debug, Clone)]
pub struct PanZoom<'a, Out, I: Image<CPoint, Out> + 'a> {
    pan: CPoint,
    zoom: f64,
    inner: I,
    marker: std::marker::PhantomData<&'a Out>,
}

impl<'a, Out, I: Image<CPoint, Out> + 'a> PanZoom<'a, Out, I> {
    fn new(pan: CPoint, zoom: f64, inner: I) -> Self {
        PanZoom {
            pan,
            zoom,
            inner,
            marker: std::marker::PhantomData {},
        }
    }

    fn zoom(&mut self, factor: f64) {
        self.zoom *= factor;
    }

    fn pan(&mut self, dx: f64, dy: f64) {
        let dx = dx / self.zoom;
        let dy = dy / self.zoom;
        self.pan = self.pan - CPoint::new(dx, dy);
    }
}

impl<'a, Out: Clone, I: Image<CPoint, Out> + 'a> Image<CPoint, Out> for PanZoom<'a, Out, I> {
    fn sample(&self, p: CPoint) -> Out {
        scale(&translate(&self.inner, self.pan), self.zoom).sample(p)
    }
}

fn four_times<'a, Out: Format, I: Image<CPoint, Out> + 'a>(
    image: &'a I,
) -> impl Image<CPoint, Out> + 'a {
    let image = quad(image, image, image, image);
    image
}

#[derive(Clone, Debug)]
pub struct Bezier3 {
    controls: [CPoint; 4],
    coeffs: [CPoint; 4],
    bb: [f64; 4],
    thickness: f64,
}

impl Bezier3 {
    fn new(e1: CPoint, c1: CPoint, c2: CPoint, e2: CPoint, thickness: f64) -> Self {
        let controls = [e1, c1, c2, e2];
        Bezier3 {
            controls,
            coeffs: Bezier3::coeffs(controls),
            bb: Bezier3::bb(controls),
            thickness: thickness * 0.5,
        }
    }

    fn coeffs(controls: [CPoint; 4]) -> [CPoint; 4] {
        let a = -controls[0] + controls[1] * 3.0 - controls[2] * 3.0 + controls[3];
        let b = controls[0] * 3.0 - controls[1] * 6.0 + controls[2] * 3.0;
        let c = -controls[0] * 3.0 + controls[1] * 3.0;
        let d = controls[0];
        [a, b, c, d]
    }

    fn bb(controls: [CPoint; 4]) -> [f64; 4] {
        let xs = &[controls[0].x, controls[1].x, controls[2].x, controls[3].x];
        let ys = &[controls[0].y, controls[1].y, controls[2].y, controls[3].y];
        [
            *xs.iter().min_by(|a, b| a.total_cmp(b)).unwrap(),
            *xs.iter().max_by(|a, b| a.total_cmp(b)).unwrap(),
            *ys.iter().min_by(|a, b| a.total_cmp(b)).unwrap(),
            *ys.iter().max_by(|a, b| a.total_cmp(b)).unwrap(),
        ]
    }

    fn at(&self, t: f64) -> CPoint {
        /*let a = self.controls[0]
            .lerp(self.controls[1], t)
            .lerp(self.controls[1].lerp(self.controls[2], t), t);
        let b = self.controls[1]
            .lerp(self.controls[2], t)
            .lerp(self.controls[2].lerp(self.controls[3], t), t);
        a.lerp(b, t)*/

        /*self.controls[0] * (1.0 - t) * (1.0 - t) * (1.0 - t)
        + self.controls[1] * 3.0 * (1.0 - t) * (1.0 - t) * t
        + self.controls[2] * 3.0 * (1.0 - t) * t * t
        + self.controls[3] * t.powi(3)*/

        self.coeffs[3] + (self.coeffs[2] + (self.coeffs[1] + self.coeffs[0] * t) * t) * t
    }

    fn distance_sq(&self, p: CPoint) -> f64 {
        const N: usize = 2;
        const DT: f64 = 1.0 / ((N - 1) as f64);

        let clos = (0..N)
            .into_iter()
            .map(|i| DT * i as f64)
            .map(|a| {
                let clos = self.converge_closest_point(p, a, 0.1, 0.001);
                (clos, self.at(clos))
            })
            .map(|(t, cl)| (t, (cl - p).len_sq()))
            .min_by(|a, b| (a.1).total_cmp(&(b.1)))
            .unwrap();

        (self.at(self.converge_closest_point(p, clos.0, 0.001, 0.00002)) - p).len_sq()
    }

    fn converge_closest_point(&self, p: CPoint, t: f64, dt: f64, threshold: f64) -> f64 {
        let mut closest_t = t;
        let closest_p = self.at(t);
        let mut closest_dist = (closest_p - p).len();

        let mut dt = dt;
        while dt > threshold {
            let before_t = closest_t - dt;
            let before_p = self.at(before_t);
            let before_dist = (before_p - p).len();
            if before_t >= 0.0 && before_dist < closest_dist {
                closest_t = before_t;
                // closest_p = before_p;
                closest_dist = before_dist;
            } else {
                let after_t = closest_t + dt;
                let after_p = self.at(after_t);
                let after_dist = (after_p - p).len();
                if after_t <= 1.0 && after_dist < closest_dist {
                    closest_t = after_t;
                    // closest_p = after_p;
                    closest_dist = after_dist;
                } else {
                    dt *= 0.5;
                }
            }
        }
        closest_t
    }
}

impl Image<CPoint, bool> for Bezier3 {
    fn sample(&self, p: CPoint) -> bool {
        if p.x <= self.bb[0] - self.thickness
            || p.x >= self.bb[1] + self.thickness
            || p.y <= self.bb[2] - self.thickness
            || p.y >= self.bb[3] + self.thickness
        {
            false
        } else if self.distance_sq(p) <= self.thickness * self.thickness {
            true
        } else {
            false
        }
    }
}

fn main() {
    let p = Poly::new(&[2.0, 2.0, 0.0, 5.0]);
    println!("{}", p);
    println!("{}", p.deriv());

    // return;

    let beginning = time::Instant::now();

    let ctx = sdl2::init().unwrap();
    let video = ctx.video().unwrap();

    let window = video
        .window("Hello, Image!", 800, 800)
        .opengl()
        .resizable()
        .allow_highdpi()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().present_vsync().build().unwrap();

    let mut event_pump = ctx.event_pump().unwrap();

    let mut mouse_pos = (0.0, 0.0);

    let mut needs_update = false;

    let mut output = canvas.output_size().unwrap();
    let mut aspect = (output.0 as f64) / (output.1 as f64);
    let mut pixs = output.1;
    let mut pixel_size = 1.0 / (pixs as f64);
    let mut sc = 1.0;

    let tc = canvas.texture_creator();
    let mut tex = tc
        .create_texture_streaming(None, (pixs as f64 * aspect) as u32, pixs)
        .unwrap();

    let mut pan = CPoint::new(0.0, 0.0);
    let mut zoom = 1.0;

    let grad3 = gradient([
        (0.0, RGBA::hex("B99095")),
        (0.1, RGBA::hex("#FCB5AC")),
        (0.2, RGBA::hex("#B5E5CFFF")),
        (0.9, RGBA::hex("3D5B59FF")),
        (0.99, RGBA::hex("333333")),
        (std::f64::INFINITY, RGBA::BLACK),
    ]);
    let grad4 = gradient([
        (0.0, RGBA([0.0, 0.0, 0.0, 1.0])),
        (0.1, RGBA([1.0, 0.0, 0.0, 1.0])),
        (0.5, RGBA([0.0, 1.0, 0.0, 1.0])),
        (1.0, RGBA([0.0, 0.0, 1.0, 1.0])),
    ]);
    let b1 = Bezier3::new(
        CPoint::new(-1.0, -0.0),
        CPoint::new(2.0, 2.0),
        CPoint::new(-2.0, 2.0),
        CPoint::new(1.0, -0.0),
        0.2,
    );
    let img = |pan, zoom, n: usize, point: Complex64| {
        PanZoom::new(
            pan,
            zoom,
            /*mix(
            or(
                b1.clone(),
                Bezier3::new(
                    CPoint::new(-1.0, -0.0),
                    CPoint::new(2.0, -2.0),
                    CPoint::new(-2.0, -2.0),
                    CPoint::new(1.0, -0.0),
                    0.2,
                ),
            ),*/
            mandelbrot_n(&grad3, n),
            // julia(CPoint::new(point.re(), point.im()), &grad4),
            // ),
        )
    };

    let mut n = 100;

    'mainloop: loop {
        let mut save: bool = false;

        while let Some(event) = event_pump.poll_event() {
            match event {
                Event::Window {
                    timestamp: _,
                    window_id: _,
                    win_event,
                } => match win_event {
                    WindowEvent::Close => {
                        break 'mainloop;
                    }
                    WindowEvent::Resized(width, height) => {
                        output = canvas.output_size().unwrap();
                        aspect = (output.0 as f64) / (output.1 as f64);
                        pixs = output.1;
                        pixel_size = 1.0 / (pixs as f64);
                        tex = tc
                            .create_texture_streaming(None, (pixs as f64 * aspect) as u32, pixs)
                            .unwrap();
                        needs_update = true;
                    }
                    WindowEvent::Shown => {
                        needs_update = true;
                    }
                    _ => {}
                },
                Event::Quit { timestamp: _ } => {
                    break 'mainloop;
                }
                Event::MouseMotion {
                    timestamp: _,
                    window_id: _,
                    which: _,
                    mousestate,
                    x,
                    y,
                    xrel,
                    yrel,
                } => {
                    mouse_pos.0 = ((x as f32) / (output.0 as f32) - 0.5) * aspect as f32;
                    mouse_pos.1 = -(y as f32) / (output.1 as f32) + 0.5;

                    if mousestate.is_mouse_button_pressed(sdl2::mouse::MouseButton::Left) {
                        pan = pan
                            + CPoint::new(
                                (xrel as f64 / output.0 as f64) / zoom * 2.0,
                                -(yrel as f64 / output.1 as f64) / zoom * 2.0,
                            );
                    }
                    needs_update = true;
                }

                Event::MouseWheel {
                    timestamp,
                    window_id,
                    which,
                    x,
                    y,
                    direction,
                } => {
                    sc *= (1.0 + y as f64 * 0.5);
                    zoom *= (1.0 + y as f64 * 0.5);
                    needs_update = true;
                }
                Event::KeyDown {
                    timestamp,
                    window_id,
                    keycode,
                    scancode,
                    keymod,
                    repeat,
                } => {
                    if let Some(kc) = keycode {
                        match kc {
                            Keycode::Escape | Keycode::Q => {
                                break 'mainloop;
                            }
                            Keycode::S => {
                                save = true;
                                needs_update = true;
                            }
                            Keycode::Up => {
                                n += 10;
                                println!("n set to {}", n);
                            }
                            Keycode::Down => {
                                n -= 10;
                                println!("n set to {}", n);
                            }
                            _ => {}
                        }
                    }
                }
                _ => {}
            }
        }
        let mouse_pos: CPoint = CPoint::new(mouse_pos.0 as f64, mouse_pos.1 as f64); // mouse_pos.into();

        needs_update = true;

        if needs_update {
            let before = time::Instant::now();

            /*
                        let inner = Box::new(scale(
                            lerp(
                                wav_dist,
                                color(RGBA([1.0, 0.0, 0.0, 1.0])),
                                color(RGBA([0.0, 1.0, 0.0, 1.0])),
                            ),
                            3.0,
                        ));

                        let inner = color_disc(RGBA::BLACK, Point::new((0.0, 0.0)), 0.5);

                        let q = quad(inner.clone(), inner.clone(), inner.clone(), inner.clone());

                        let q2 = quad(q.clone(), q.clone(), q.clone(), q.clone());

                        let q3 = quad(q2.clone(), q2.clone(), q.clone(), q.clone());

                        let sc = 3.0;

                        let img = blend(q3, white);
                        let img = quad(
                            img.clone(),
                            local_avg(img.clone(), 0.5 * pixel_size * sc, 0.5 * pixel_size * sc),
                            local_avg(img.clone(), pixel_size * sc, pixel_size * sc),
                            local_avg(img.clone(), 2.0 * pixel_size * sc, 2.0 * pixel_size * sc),
                        );
                        let img = frame(img, mouse_pos, mouse_pos + Point::new((1.0, 1.0)));
            */

            let time = before.duration_since(beginning).as_secs_f64() * 0.1;

            let point = Complex::exp(Complex::<f64>::i() * time) * 0.7885;
            /*
            let grad = gradient([
                (0.0, RGBA::bytes(0xB9, 0x90, 0x95, 0xFF)),
                (0.1, RGBA::bytes(0xFC, 0xB5, 0xAC, 0xFF)),
                (0.2, RGBA::bytes(0xB5, 0xE5, 0xCF, 0xFF)),
                (1.0, RGBA::bytes(0x3D, 0x5B, 0x59, 0xFF)),
                (std::f64::INFINITY, RGBA::BLACK),
            ]);
            let img = frame(
                PanZoom::new(
                    pan,
                    zoom,
                    mix(
                        scale(checkerboard, 0.0003 / zoom),
                        julia(
                            CPoint::new(point.re(), point.im()),
                            gradient([
                                (0.0, RGBA([0.0, 0.0, 0.0, 1.0])),
                                (0.1, RGBA([1.0, 0.0, 0.0, 1.0])),
                                (0.5, RGBA([0.0, 1.0, 0.0, 1.0])),
                                (1.0, RGBA([0.0, 0.0, 1.0, 1.0])),
                            ]),
                        ),
                        mandelbrot(grad.clone()),
                    ),
                ),
                // mandelbrot(gradient(&[(0.0, RGBA::BLACK), (1.0, RGBA::WHITE)])),
                // mandelbrot(simple_gradient(RGBA::BLACK, RGBA::WHITE)),
                CPoint::new(-1.0, -1.0),
                CPoint::new(1.0, 1.0),
            );*/
            /*
            let img = translate(
                scale(
                    mix(
                        scale(checkerboard, 0.1),
                        scale(mandelbrot(simple_gradient(RGBA::BLACK, RGBA::WHITE)), 1.0),
                        // bilerp_color(RGBA::BLACK, RGBA::RED, RGBA::BLUE, RGBA::WHITE),
                        scale(mandelbrot(simple_gradient(RGBA::WHITE, RGBA::BLACK)), 1.0),
                    ),
                    2.0,
                ),
                Point::new((-1.0, -1.0)),
            );
            */
            let grad2 = gradient([
                (0.0, RGBA::bytes(0x62, 0x4B, 0x95, 0xFF)),
                (1.0, RGBA::bytes(0x95, 0x4B, 0x59, 0xFF)),
                (2.0, RGBA::bytes(0x7E, 0x95, 0x4B, 0xFF)),
                (3.0, RGBA::bytes(0x4B, 0x95, 0x88, 0xFF)),
                (std::f64::INFINITY, RGBA::BLACK),
            ]);
            /*
                        let img = newton_raphson(
                            grad,
                            Polynomial::new(&[
                                Complex64::new(1.0, 0.0),
                                Complex64::new(0.0, 1.0),
                                Complex64::new(0.0, -1.0),
                                Complex64::new(-1.0, 0.0),
                            ]),
                            50,
                        );
            */
            /*let img = mix(
                scale(checkerboard, 0.01),
                color(RGBA::WHITE),
                color(RGBA::BLACK),
            );

            let img = PanZoom::new(
                pan,
                zoom,
                mandelbrot(&grad3), // julia(CPoint::new(-0.8, 0.156), &grad3),
            );*/
            let scale = 1.0;
            let img = img(pan, zoom, n, point);

            // let img = color(RGBA::BLACK);
            write_image_to_sdl2_texture(
                &img,
                &mut tex,
                Quad::new(
                    ((-aspect * 1.0) * scale, (-1.0) * scale),
                    ((aspect * 1.0) * scale, (-1.0) * scale),
                    ((-aspect * 1.0) * scale, (1.0) * scale),
                    ((aspect * 1.0) * scale, (1.0) * scale),
                ),
            );

            // println!("{:?}", output);

            if save {
                let tex = Texture::from_sampled_image::<_, _, SRgba8>(
                    output.0 as usize,
                    output.1 as usize,
                    &img,
                    Quad::new(
                        ((-aspect * 1.0) * scale, (-1.0) * scale),
                        ((aspect * 1.0) * scale, (-1.0) * scale),
                        ((-aspect * 1.0) * scale, (1.0) * scale),
                        ((aspect * 1.0) * scale, (1.0) * scale),
                    ),
                );
                let now: DateTime<Utc> = Utc::now();
                tex.write_to_file(format!("out {}.png", now));
            }

            canvas.clear();
            canvas.copy(&tex, None, None).unwrap();
            canvas.present();
            let after = time::Instant::now();
            println!("Draw took {}ms", (after - before).as_millis());
            needs_update = false;
        }
    }
}
