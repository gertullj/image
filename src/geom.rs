use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct CPoint {
    pub x: f64,
    pub y: f64,
}

impl CPoint {
    pub fn new(x: f64, y: f64) -> Self {
        CPoint { x, y }
    }

    pub fn lerp(&self, other: CPoint, t: f64) -> CPoint {
        CPoint {
            x: (1.0 - t) * self.x + t * other.x,
            y: (1.0 - t) * self.y + t * other.y,
        }
    }

    pub fn len(&self) -> f64 {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    pub fn len_sq(&self) -> f64 {
        (self.x * self.x + self.y * self.y)
    }
}

impl std::ops::Add<CPoint> for CPoint {
    type Output = CPoint;
    fn add(self, rhs: Self) -> Self {
        CPoint {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::ops::Sub<CPoint> for CPoint {
    type Output = CPoint;
    fn sub(self, rhs: Self) -> Self {
        CPoint {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl std::ops::Neg for CPoint {
    type Output = CPoint;
    fn neg(self) -> Self {
        CPoint {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl std::ops::Mul<f64> for CPoint {
    type Output = CPoint;
    fn mul(self, rhs: f64) -> Self {
        CPoint {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl From<(f64, f64)> for CPoint {
    fn from(other: (f64, f64)) -> Self {
        CPoint::new(other.0, other.1)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct PPoint {
    pub r: f64,
    pub theta: f64,
}

impl PPoint {
    pub fn new(r: f64, theta: f64) -> Self {
        PPoint { r, theta }
    }

    pub fn lerp(&self, other: PPoint, t: f64) -> PPoint {
        let p1: CPoint = (*self).into();
        let p2: CPoint = (other).into();
        let p3 = p1.lerp(p2, t);
        PPoint::from(p3)
    }
}

impl std::ops::Add<PPoint> for PPoint {
    type Output = PPoint;

    fn add(self, rhs: Self) -> Self {
        let cos_rt_lt = (rhs.theta - self.theta).cos();
        let sin_rt_lt = (rhs.theta - self.theta).sin();
        PPoint {
            r: (self.r * self.r + rhs.r * rhs.r + 2.0 * self.r * rhs.r * cos_rt_lt).sqrt(),
            theta: self.theta + self.r * sin_rt_lt.atan2(self.r * rhs.r * cos_rt_lt),
        }
    }
}

impl std::ops::Sub<PPoint> for PPoint {
    type Output = PPoint;
    fn sub(self, rhs: Self) -> Self {
        self + (-rhs)
    }
}

impl std::ops::Neg for PPoint {
    type Output = PPoint;
    fn neg(self) -> Self {
        PPoint {
            r: self.r,
            theta: -self.theta,
        }
    }
}

impl std::ops::Mul<f64> for PPoint {
    type Output = PPoint;
    fn mul(self, rhs: f64) -> Self {
        PPoint {
            r: self.r * rhs,
            theta: self.theta,
        }
    }
}

impl From<(f64, f64)> for PPoint {
    fn from(other: (f64, f64)) -> Self {
        PPoint::new(other.0, other.1)
    }
}

impl From<CPoint> for PPoint {
    fn from(point: CPoint) -> PPoint {
        PPoint {
            r: (point.x * point.x + point.y * point.y).sqrt(),
            theta: point.y.atan2(point.x),
        }
    }
}

impl From<PPoint> for CPoint {
    fn from(point: PPoint) -> CPoint {
        CPoint {
            x: point.r * point.theta.cos(),
            y: point.r * point.theta.sin(),
        }
    }
}

pub trait Sample: Copy + Add + Sub + Neg + Mul<f64> {}
impl Sample for CPoint {}
impl Sample for PPoint {}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Quad([CPoint; 4]);

impl Quad {
    pub fn new(p0: (f64, f64), p1: (f64, f64), p2: (f64, f64), p3: (f64, f64)) -> Self {
        Quad([p0.into(), p1.into(), p2.into(), p3.into()])
    }

    pub fn from_points(p0: CPoint, p1: CPoint, p2: CPoint, p3: CPoint) -> Self {
        Quad([p0, p1, p2, p3])
    }

    pub fn sample(&self, tx: f64, ty: f64) -> CPoint {
        let upper = self.0[2].lerp(self.0[3], tx);
        let lower = self.0[0].lerp(self.0[1], tx);

        upper.lerp(lower, ty)
    }
}
